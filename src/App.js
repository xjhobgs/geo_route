/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import qs from "query-string";

import { makeStyles } from '@material-ui/core/styles';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import TextField from '@material-ui/core/TextField';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';

import { useInput } from './hooks/input-hook';
import Table from './tables';
import Upload from './uploads';
import configs from './configs';
import './App.css';

const useStyles = makeStyles((theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  }
}));

const App = () => {
  const theme = React.useMemo(
    () =>
      createMuiTheme({
        palette: {
          type: 'dark',
        },
      }),
    [],
  );
  const classes = useStyles();

  const { value: client_id, setValue: setClientID, bind: bindClientID } = useInput('');
  const { value: client_secret, setValue: setClientSecret, bind: bindClientSecret } = useInput('');
  const { value: service_url, setValue: setServiceUrl, bind: bindServiceUrl } = useInput('');
  const [authenticationError, setAuthenticationError] = useState({
    hasError: false,
    message: ''
  });
  const [formAuthenticateSuccess, setFormAuthenticateSuccess] = useState(false);
  const [addRoutesResults, setAddRoutesResults] = useState({
    hasError: false,
    isSuccess: false,
    message: ''
  });
  const [access_token, setAccessToken] = useState('');
  const [expire_time, setExpireTime] = useState(null);
  const [uploadedData, setUploadedData] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const access_token_store = localStorage.getItem('access_token');

    if (access_token_store) {
      const expire_time_store = localStorage.getItem('expire_time');

      if (expire_time_store) {
        setAccessToken(access_token_store);
        setExpireTime(expire_time_store);

        if (!isExpired(parseFloat(expire_time_store))) {
          setFormAuthenticateSuccess(true);
        } else {
          setAuthenticationError({
            hasError: true,
            message: ''
          });
          setFormAuthenticateSuccess(false);
        }

        const client_id_store = localStorage.getItem('client_id');
        if (client_id_store) {
          setClientID(client_id_store);
        }

        const client_secret_store = localStorage.getItem('client_secret');
        if (client_secret_store) {
          setClientSecret(client_secret_store);
        }

        const service_url_store = localStorage.getItem('service_url');
        if (service_url_store) {
          setServiceUrl(service_url_store);
        }
      }
    }
  }, []);

  const showLoading = () => {
    setLoading(true);
  };

  const hideLoading = () => {
    setLoading(false);
  };

  const submitArcGisAuthenticationForm = (event) => {
    event.preventDefault();

    showLoading();

    axios.post(configs.authenticateApiUrl, {
      client_id: client_id,
      client_secret: client_secret,
      service_url: service_url
    })
      .then(response => {
        setAccessToken(response.data.token.access_token);
        setExpireTime(response.data.token.expire_time);
        localStorage.setItem('client_id', client_id);
        localStorage.setItem('client_secret', client_secret);
        localStorage.setItem('service_url', service_url);
        localStorage.setItem('access_token', response.data.token.access_token);
        localStorage.setItem('expire_time', response.data.token.expire_time);
        setAuthenticationError({
          hasError: false,
          message: ''
        });
        setFormAuthenticateSuccess(true);

        hideLoading();
      })
      .catch(error => {
        setAccessToken('');
        setExpireTime(null);
        localStorage.setItem('client_id', '');
        localStorage.setItem('client_secret', '');
        localStorage.setItem('service_url', service_url);
        localStorage.setItem('access_token', '');
        localStorage.setItem('expire_time', null);
        setAuthenticationError({
          hasError: true,
          message: error.response.data.message
        });
        setFormAuthenticateSuccess(false);

        hideLoading();
      });
  }

  const onUpload = newUploadedData => {
    setUploadedData(newUploadedData);
  }

  const chunkArray = (array, chunkSize) => {
    const results = [];
    const newArray = array.slice(0);

    while (newArray.length) {
      results.push(newArray.splice(0, chunkSize));
    }

    return results;
  }

  const createRoutes = () => {
    showLoading();

    const uploadedDataArray = chunkArray(uploadedData, configs.createRoutesRecordLimitPerBatch);

    new Promise((resolve, reject) => {
      const axiosArray = [];

      for (let i = 0; i < uploadedDataArray.length; i++) {
        axiosArray.push(axios({
          url: configs.createRoutesApiUrl,
          method: 'POST',
          headers: { 'content-type': 'application/x-www-form-urlencoded' },
          data: qs.stringify({
            routes: JSON.stringify(
              uploadedDataArray[i]
            ),
            client_id: client_id,
            client_secret: client_secret,
            service_url: service_url,
            access_token: access_token,
            expire_time: expire_time
          })
        }));
      }

      axios
        .all(axiosArray)
        .then(
          axios.spread((...responses) => {
            setAddRoutesResults({
              hasError: false,
              isSuccess: true,
              message: responses.map(x => x.data)
            });

            resolve(responses);
            hideLoading();
          })
        )
        .catch(errors => {
          setAddRoutesResults({
            hasError: true,
            isSuccess: false,
            message: errors
          });

          reject(errors);
          hideLoading();
        });
    });
  }

  const isExpired = (time) => {
    return (new Date(time)) <= (new Date());
  }

  const renderResponseAccessToken = () => {
    if (access_token) {
      if (isExpired(parseFloat(expire_time))) {
        return (
          <span className="wordbreak error">
            Access Token: <Icon className="error font-size-14">error</Icon> Expired. Please submit again.
          </span>
        )
      } else {
        return (
          <span className="wordbreak">
            Access Token: <Icon className="success font-size-14">check_circle</Icon>
          </span>
        )
      }
    }
    else {
      return;
    }
  }

  const renderResponseExpireIn = () => {
    const parsed_expire_time = parseFloat(expire_time);

    if (parsed_expire_time) {
      if (isExpired(parsed_expire_time)) {
        return (
          <span className="wordbreak error">Expires In: {(new Date(parsed_expire_time)).toDateString()}</span>
        )
      } else {
        return (
          <span className="wordbreak">Expires In: {(new Date(parsed_expire_time)).toUTCString()}</span>
        )
      }
    }
    else {
      return;
    }
  }

  const renderAddRoutesResponseContent = () => {
    return (
      <div className={addRoutesResults.hasError ? 'add-routes-response-content fail-submit' : (addRoutesResults.isSuccess ? 'add-routes-response-content success-submit' : 'add-routes-response-content')}>
        <span className="underline">Response:</span>

        <br />

        <pre className={addRoutesResults.hasError ? 'json-view wordbreak error' : 'json-view wordbreak'}>{JSON.stringify(addRoutesResults.message, null, 2)}</pre>
      </div>
    )
  }

  const renderTable = () => {
    if (uploadedData && uploadedData.length > 0) {
      return (
        <div className="table-content">
          <Table rows={uploadedData}></Table>

          <Button
            variant="contained"
            color="primary"
            endIcon={<Icon>send</Icon>}
            className="publish-button"
            disabled={uploadedData.length <= 0}
            onClick={createRoutes}
          >
            Publish
          </Button>

          {renderAddRoutesResponseContent()}
        </div>
      )
    } else {
      return;
    }
  }

  return (
    <ThemeProvider theme={theme}>
      <div className="App">
        <h4 className="title-header underline">Georoute</h4>

        <ExpansionPanel
          defaultExpanded
          className={authenticationError.hasError ? 'arcgis-online-authentication-panel error' : (formAuthenticateSuccess ? 'arcgis-online-authentication-panel success' : 'arcgis-online-authentication-panel')}>
          <ExpansionPanelSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="arcgis-authentication-content"
            id="arcgis-authentication-header"
            className="cursor-pointer"
          >
            <Typography className={authenticationError.hasError ? 'header underline error' : (formAuthenticateSuccess ? 'header underline success' : 'header underline')}>
              ArcGIS Online Authentication
            </Typography>
          </ExpansionPanelSummary>

          <ExpansionPanelDetails>
            <form
              className="ArcGIS-Authentication-form"
              autoComplete="off"
              onSubmit={submitArcGisAuthenticationForm}>
              <div className="form-content">
                <div>
                  <TextField
                    id="client_id"
                    name="client_id"
                    label="Client ID"
                    className="input-field"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    required
                    {...bindClientID} />

                  <TextField
                    id="client_secret"
                    name="client_secret"
                    label="Client Secret"
                    type="password"
                    className="input-field"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    required
                    {...bindClientSecret} />
                </div>

                <div>
                  <TextField
                    id="service_url"
                    name="service_url"
                    label="Feature Layer Service Url"
                    className="input-field full"
                    margin="normal"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    required
                    {...bindServiceUrl}
                  />
                </div>
              </div>

              <div className={authenticationError.hasError ? 'response-content fail-submit' : (formAuthenticateSuccess ? 'response-content success-submit' : 'response-content')}>
                <span className="underline">Response:</span>

                <br />
                {renderResponseAccessToken()}

                <br />

                {renderResponseExpireIn()}

                <span className="error">{authenticationError.message}</span>
              </div>

              <div className="button-content">
                <Button
                  variant="contained"
                  color="primary"
                  endIcon={<Icon>send</Icon>}
                  type="submit"
                >
                  Submit
                </Button>
              </div>
            </form>
          </ExpansionPanelDetails>
        </ExpansionPanel>

        <ExpansionPanel
          className="file-upload-panel"
          disabled={authenticationError.hasError || !formAuthenticateSuccess}
        >
          <ExpansionPanelSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="file-upload-content"
            id="file-upload-header"
            className="cursor-pointer"
          >
            <Typography className='header underline'>
              File Upload
            </Typography>
          </ExpansionPanelSummary>

          <ExpansionPanelDetails className="file-upload-content-details">
            <Upload onUpload={onUpload}></Upload>

            {renderTable()}
          </ExpansionPanelDetails>
        </ExpansionPanel>
      </div>

      <Backdrop className={classes.backdrop} open={loading}>
        <CircularProgress color="inherit" />
      </Backdrop>
    </ThemeProvider >
  );
}

export default App;