import React, { useState } from 'react';
import axios from 'axios';

import RadioButtonUncheckedIcon from '@material-ui/icons/RadioButtonUnchecked';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';

import Dropzone from './dropzone';
import configs from '../configs';
import './index.css';

const Upload = ({ onUpload }) => {
    const [file, setFile] = useState(null);
    const [uploading, setUploading] = useState(false);
    const [successfullUploaded, setSuccessfullUploaded] = useState(false);
    const [errorUpload, setErrorUpload] = useState({
        hasError: false,
        message: ''
    });

    const onFileAdded = newFile => {
        setFile(newFile);
    }

    const sendRequest = file =>
        new Promise((resolve, reject) => {
            const formData = new FormData();
            formData.append('file', file);

            axios
                .post(configs.uploadApiUrl, formData, {})
                .then(res => {
                    resolve(res);
                })
                .catch(err => {
                    reject(err);
                })
        });

    const uploadFile = async () => {
        setUploading(true);
        setSuccessfullUploaded(false);
        setErrorUpload({
            hasError: false,
            message: ''
        });

        await sendRequest(file)
            .then(res => {
                onUpload(res.data);
                setSuccessfullUploaded(true);
                setUploading(false);
            })
            .catch(err => {
                setErrorUpload({
                    hasError: true,
                    message: err
                });
                setUploading(false);
            });
    }

    const renderActions = () => {
        return (
            <Button
                style={{ backgroundColor: "#999999" }}
                variant="outlined"
                color="primary"
                endIcon={<Icon>publish</Icon>}
                disabled={!file || uploading}
                onClick={uploadFile}
            >
                Upload
            </Button>
        );
    }

    const renderFiles = () => {
        if (file) {
            if (!uploading && successfullUploaded && !errorUpload.hasError) {
                return (
                    <div key={file.name} className="Row success">
                        <CheckCircleOutlineIcon />
                        <span className="Filename">{file.name}</span>
                    </div>
                );
            } else if (!uploading && !successfullUploaded && errorUpload.hasError) {
                return (
                    <div key={file.name} className="Row error" title={errorUpload.message}>
                        <ErrorOutlineIcon />
                        <span className="Filename">{file.name}</span>
                    </div>
                );
            } else if (!successfullUploaded && !errorUpload.hasError) {
                return (
                    <div key={file.name} className="Row">
                        <RadioButtonUncheckedIcon />
                        <span className="Filename">{file.name}</span>
                    </div>
                );
            }
        }
    }

    const download = (event) => {
        event.preventDefault();

        axios({
            url: configs.downloadTemplateApiUrl,
            method: 'GET',
            responseType: 'blob',
        })
            .then((response) => {
                const url = window.URL.createObjectURL(new Blob([response.data], {
                    type: 'application/octet-stream',
                    responseType: 'arraybuffer'
                }));
                const link = document.createElement('a');

                link.href = url;
                link.setAttribute('download', 'Template.csv');
                document.body.appendChild(link);
                link.click();
            });
    }

    return (
        <div className="Upload">
            <div className="Content">
                <Button
                    style={{ margin: '0 24px 15px 0' }}
                    variant="contained"
                    color="primary"
                    endIcon={<Icon>get_app</Icon>}
                    onClick={download}
                >
                    Template Download
                </Button>

                <div>
                    <Dropzone
                        onFileAdded={onFileAdded}
                        disabled={uploading || successfullUploaded}
                    />
                </div>

                <div className="Files">{renderFiles()}</div>

                <div className="Actions">{renderActions()}</div>
            </div>
        </div>
    );
}

export default Upload;