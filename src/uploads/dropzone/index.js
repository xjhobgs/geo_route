import React, { useState, useRef } from 'react';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import './index.css';

const acceptedFileFormats = ".csv";

const Dropzone = ({ disabled, onFileAdded }) => {
    const [disabledProp] = useState(disabled);
    const [highlight] = useState(false);

    const fileInputRef = useRef(null);

    const onFileAddedProp = evt => {
        if (disabledProp) return;

        const file = evt.target.files[0];

        if (onFileAdded) {
            onFileAdded(file);
        }
    }

    const openFileDialog = () => {
        if (disabledProp) return;
        fileInputRef.current.value = null;
        fileInputRef.current.click();
    }

    return (
        <div
            className={`Dropzone ${highlight ? "Highlight" : ""}`}
            onClick={openFileDialog}
            style={{ cursor: disabledProp ? "default" : "pointer" }}
        >
            <input
                ref={fileInputRef}
                className="FileInput"
                type="file"
                accept={acceptedFileFormats}
                onChange={onFileAddedProp}
            />

            <CloudUploadIcon />
        </div>
    )
}

export default Dropzone;