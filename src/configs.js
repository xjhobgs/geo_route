// DEV
// module.exports = {
//     authenticateApiUrl: 'http://localhost:3050/authenticate',
//     uploadApiUrl: 'http://localhost:3050/upload',
//     createRoutesApiUrl: 'http://localhost:3050/createRoutes',
//     downloadTemplateApiUrl: 'http://localhost:3050/downloadTemplate',
//     createRoutesRecordLimitPerBatch: 10
// };

// PROD
module.exports = {
    authenticateApiUrl: 'https://kufuq75bu4.execute-api.ap-southeast-2.amazonaws.com/dev/authenticate',
    uploadApiUrl: 'https://kufuq75bu4.execute-api.ap-southeast-2.amazonaws.com/dev/upload',
    createRoutesApiUrl: 'https://kufuq75bu4.execute-api.ap-southeast-2.amazonaws.com/dev/createRoutes',
    downloadTemplateApiUrl: 'https://kufuq75bu4.execute-api.ap-southeast-2.amazonaws.com/dev/downloadTemplate',
    createRoutesRecordLimitPerBatch: 5
};