import React, { useState } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import './index.css';

const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
    root: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        },
    },
}))(TableRow);

const columns = [
    { id: 'ProjectYear', label: 'Project Year', minWidth: 100 },
    { id: 'StreetName', label: 'Street Name', minWidth: 100 },
    { id: 'StartCrossStreet', label: 'Start Cross Street', minWidth: 100 },
    { id: 'EndCrossStreet', label: 'End Cross Street', minWidth: 100 },
    { id: 'Area', label: 'Area (sqm)', minWidth: 100 },
    { id: 'ProposedYear', label: 'Proposed Year', minWidth: 100 },
    { id: 'Total', label: 'Total ($)', minWidth: 100 },
    { id: 'Suburb', label: 'Suburb', minWidth: 100 },
    { id: 'Ward', label: 'Ward', minWidth: 100 },
    { id: 'YearlyArea', label: 'Yearly Area (m2)', minWidth: 100 },
    { id: 'YearlyTotal', label: 'Yearly Total($)', minWidth: 100 },
    { id: 'State', label: 'State', minWidth: 100 }
];

const StickyHeadTable = (props) => {
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(100);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    }

    const handleChangeRowsPerPage = event => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    }

    return (
        <div className="Table-paper">
            <TableContainer className="Table-container">
                <Table stickyHeader aria-label="sticky table">
                    <TableHead>
                        <TableRow>
                            {columns.map((column) => (
                                <StyledTableCell
                                    key={column.id}
                                    align={column.align}
                                    style={{ minWidth: column.minWidth }}
                                >
                                    {column.label}
                                </StyledTableCell>
                            ))}
                        </TableRow>
                    </TableHead>

                    <TableBody>
                        {props.rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row, index) => {
                            return (
                                <StyledTableRow hover role="checkbox" tabIndex={-1} key={index}>
                                    {columns.map((column) => {
                                        const value = row[column.id];
                                        return (
                                            <TableCell key={column.id} align={column.align}>
                                                {column.format && typeof value === 'number' ? column.format(value) : value}
                                            </TableCell>
                                        );
                                    })}
                                </StyledTableRow>
                            );
                        })}
                    </TableBody>
                </Table>
            </TableContainer>

            <TablePagination
                rowsPerPageOptions={[25, 50, 100]}
                component="div"
                count={props.rows.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
                color="#fff"
            />
        </div>
    );
}

export default StickyHeadTable;